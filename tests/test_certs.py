"""Test cki_lib.certs module."""
import datetime
from datetime import timezone
import pathlib
import tempfile
import unittest
from unittest import mock

try:
    from cryptography import x509
    from cryptography.hazmat import primitives

    from cki_lib import certs

    NO_CRYPTOGRAPHY = False
except ImportError:
    NO_CRYPTOGRAPHY = True


@unittest.skipIf(NO_CRYPTOGRAPHY, 'cryptography is not installed')
class TestMetrics(unittest.TestCase):
    """Test cert metrics."""

    @mock.patch('cki_lib.certs.METRIC_CERTIFICATE_NOT_VALID_AFTER')
    def test_expiry(self, mock_metric) -> None:
        """Test certificate expiry."""
        expiry = datetime.datetime.utcnow().replace(microsecond=0) + datetime.timedelta(days=10)
        with tempfile.NamedTemporaryFile() as temp:
            key = primitives.asymmetric.rsa.generate_private_key(65537, 2048)
            cert = (
                x509.CertificateBuilder()
                    .subject_name(x509.Name([]))
                    .issuer_name(x509.Name([]))
                    .public_key(key.public_key())
                    .serial_number(x509.random_serial_number())
                    .not_valid_before(datetime.datetime.utcnow())
                    .not_valid_after(expiry)
                    .sign(key, primitives.hashes.SHA256())
            )
            pathlib.Path(temp.name).write_bytes(
                cert.public_bytes(primitives.serialization.Encoding.PEM)
            )

            certs.update_certificate_metrics(temp.name)
            mock_metric.assert_has_calls([
                mock.call.labels(temp.name),
                mock.call.labels().set(expiry.replace(tzinfo=timezone.utc).timestamp()),
            ])
